''' There are vector and matrix implemented with term. Program includes test and asserts'''


from vector import *
from matrix import *
from term import *




if __name__ == "__main__":
    
    a = Vector([Term.var("A_x"),Term.var("A_b"), Term.var("A_c")])
    b = Vector([Term.var("B_x"),Term.var("B_b"), Term.var("B_c")])
    c = Vector([Term.const(1), Term.const(2), Term.const(3)])
    d = Vector([Term.const(5), Term.const(4), Term.const(3)])
    h = Matrix([Term.var("M_xx"),Term.var("M_xy"), Term.var("M_xz"),Term.var("M_yx"),Term.var("M_yy"), Term.var("A_yz"),Term.var("A_zx"),Term.var("A_zy"), Term.var("A_zz")])

    c + d
    c @ d
    5 * c
    2 * a
    a + b
    a @ b
    5 * h    
    h @ a        
    h @ h

    # vectors asserts
    assert a + b == b + a
    assert a + (b + c) == (a + b) + c 
    assert 2*(a + b) == 2*a + 2*b
    assert 5*a == (2+3)*a 
    assert 1*a == a
    assert 2*(3*a) == 3*(2*a)
    assert Vector([0,0,0]) + a == a + Vector([0,0,0]) == a

