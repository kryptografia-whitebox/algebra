def polynomial_divide(polynomial1, polynomial2):                 #Place for Polynomial Division
    pass

def polynomial_addition(polynomial1, polynomial2):               #Returns total of two polynomials
    pass

def polynomial_scalar_multiplication(polynomial, multiplier):    #Returns product of polynomial multiplied by scalar
    polynomial = [(monomial[0]*multiplier, monomial[1]) for monomial in polynomial]
    return polynomial

def normalize(polynomial):                                       #Normalizes Polynomials before division
    new_polynomial = []
    for monomial in polynomial:
        (monomial[0],''.join(sorted(monomial[1])))               #Sorting string characters from a to z
        new_polynomial.append((monomial[0],''.join(sorted(monomial[1]))) )
    new_polynomial = sorted(new_polynomial, key = lambda monomial: len(monomial[1]), reverse=True)
    return new_polynomial

def checker(old_polynomial, new_polynomial):

    if len(old_polynomial) != len(new_polynomial):                #Checking length of old and new polynomial
        return False

    for monomial in new_polynomial:                               #Checking order of letters
        variables = monomial[1]
        for element_in_monomial in range(len(variables) - 1):
            if variables[element_in_monomial]>variables[element_in_monomial + 1]:
                return False

    for monomial_index in range(len(new_polynomial) - 1):        #Checking order of tuples
        if len(new_polynomial[monomial_index]) < len(new_polynomial[monomial_index + 1]):
            return False

    for monomial in new_polynomial:
        if len(monomial) != 2:
            return False

    return True

var1 = [(1, "xaxhx"), (5, "yxygagaga"), (8, ""), (3, "hgaax")]          #(8,"") is a constant term which equals 8
var2 = normalize(var1)
var3 = [(120, "xtyuwe"), (9, "yoppqa"), (10, "da"), (5, "afaga")]
var4 = normalize(var3)
var5 = [(50, ""), (19, "uaigquia"), (12, "pjkwq"), (2, "qwoi")]
var6 = normalize(var5)
assert checker(var1, var2)
assert checker(var3,var4)
assert checker(var5,var6)
#assert checker(var1,var3)   <- This should cause program to run error
print(polynomial_scalar_multiplication(var2,10))
print(polynomial_scalar_multiplication(var4,0.5))
