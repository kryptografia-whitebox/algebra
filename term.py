'''
    File name: Term.py
    Author: Krzysztof Galus
    Date created: 10/12/2020
    Date last modified: 10/12/2020
    Python Version: 3.7
'''

__all__ = ['Term']
__author__ = "Krzysztof Galus"
__email__ = "krzysztof.galus.k7@gmail.com"
__license__ = "Public domain"

from enum import Enum
from itertools import product

class Term:
    class Type(Enum):
        VAR = 1
        CONST = 2
        ADD = 3
        SUB = 4
        MUL = 5

    def __init__(self, expression_type, variable=None, constant=None):
        self.variable = variable
        self.constant = constant
        self.tree = []
        self.expression_type = expression_type

    @classmethod
    def const(cls, constant):
        term = cls(Term.Type.CONST, None, constant)
        return term

    @classmethod
    def var(cls, variable):
        term = cls(Term.Type.VAR, variable, None)
        return term

    def __call__(self, **kwargs):
        assert self.is_valid()
        if self.is_const():
            return self.constant
        elif self.is_var():
            if self.variable in kwargs:
                return kwargs.get(self.variable)
            return kwargs.get(list(kwargs.keys())[0])
        elif self.is_add():
            return self.tree[1](**kwargs) + self.tree[2](**kwargs)
        elif self.is_sub():
            return self.tree[1](**kwargs) - self.tree[2](**kwargs)
        elif self.is_mul():
            return self.tree[1](**kwargs) * self.tree[2](**kwargs)
        raise RuntimeError("I should not be here")

    def __add__(self, other):
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self + self.const(other)
        except TypeError:
            return self + self.const(other)
        term = Term(self.Type.ADD)
        term.tree.append("+")
        term.tree.append(self)
        term.tree.append(other)
        return term

    def __sub__(self, other):
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self - self.const(other)
        except TypeError:
            return self - self.const(other)
        term = Term(self.Type.SUB)
        term.tree.append("-")
        term.tree.append(self)
        term.tree.append(other)
        return term

    def __mul__(self, other):
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self * self.const(other)
        except TypeError:
            return self * self.const(other)
        term = Term(self.Type.MUL)
        term.tree.append("*")
        term.tree.append(self)
        term.tree.append(other)
        return term

    __rmul__ = __mul__
    
    __radd__ = __add__
    
    __rsub__ = __sub__

    def __str__(self):
        assert self.is_valid()
        if self.is_const():
            return str(self.constant)
        elif self.is_var():
            return str(self.variable)
        elif self.is_add() or self.is_sub() or self.is_mul():
            return "(" + str(self.tree[1]) + self.tree[0] + str(self.tree[2]) + ")"
        raise RuntimeError("I should not be here")

    def __eq__(self, other):
        
        try:
            if self.variables() != other.variables():
                return False
        except AttributeError:
            if self == self.const(other):
                return True
            return NotImplemented

        for var in self.variables():
            if self.count(var) != other.count(var):
                return False

        for i in product([1, 0], repeat=len(self.variables())):
            if self(**dict(zip(self.variables(), i))) != other(**dict(zip(other.variables(), i))):
                return False

        return True

    def count(self, arg):
        try:
            if not self.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self.var(self).count(arg)
        except TypeError:
            return self.var(self).count(arg)
        if self.is_var() and self.variable == arg:
            return 1
        elif self.is_var() and self.variable != arg:
            return 0
        elif self.is_const():
            return 0
        elif self.is_add() or self.is_mul() or self.is_sub():
            return self.tree[1].count(arg) + self.tree[2].count(arg)

    def is_const(self):
        return self.expression_type == Term.Type.CONST

    def is_var(self):
        return self.expression_type == Term.Type.VAR

    def is_add(self):
        return self.expression_type == Term.Type.ADD

    def is_sub(self):
        return self.expression_type == Term.Type.SUB

    def is_mul(self):
        return self.expression_type == Term.Type.MUL

    def variables(self):
        assert self.is_valid()
        if self.is_const():
            return set()
        elif self.is_var():
            return set([self.variable])
        elif self.is_add() or self.is_mul() or self.is_sub():
            return self.tree[1].variables() | self.tree[2].variables()
        raise RuntimeError("I should not be here")

    def is_valid(self):
        if sum([self.is_add(), self.is_var(), self.is_const(), self.is_mul(), self.is_sub()]) != 1:
            return False
        if self.is_add() or self.is_sub() or self.is_mul():
            return self.tree[1].is_valid() and self.tree[2].is_valid()
        elif self.is_var():
            return self.variable != None
        elif self.is_const():
            return self.constant != None
        raise RuntimeError("I should not be here")


if __debug__ and __name__ == '__main__':
    x = Term.var("x")
    y = Term.var("y")
    z = Term.var("z")
    g = x + y
    f = x + y
    print(f(x=1, y=2))
    assert Term.var("x").variables() == set("x")
    assert Term.const(0).variables() == set()
    assert (x+y).variables() == {"x", "y"}
    assert (x+(y*z)).variables() == {"x", "y", "z"}
    assert f(x=1, y=2) == 3
    print(f(x=1,y=2))

    assert Term.const(7) == 7
    assert 8 == Term.const(8)
    assert Term.const(7) != 8
    assert Term.var("x") != "abc"
    assert Term.var(9) != "abc"
